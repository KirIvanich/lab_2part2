package com.example.kir.lab2_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {


    private ListView listView;
    private EditText editText;
    private Button button1;
    private ListAdapter listAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=(ListView)findViewById(R.id.list);
        editText=(EditText)findViewById(R.id.edit_text);
        button1=(Button)findViewById(R.id.set_button);
        String items[]={"Merccedes","BMW","Audi","Volvo"};
        listAdapter=new ListAdapter(new ArrayList<String>(Arrays.asList(items)),this);
        listView.setAdapter(listAdapter);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAdapter.addItem(editText.getText().toString());
                editText.setText("");
            }
        });
    }
}
