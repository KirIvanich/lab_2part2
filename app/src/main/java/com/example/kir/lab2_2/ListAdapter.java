package com.example.kir.lab2_2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kir on 06.12.2015.
 */
public class ListAdapter extends BaseAdapter
{
    private ArrayList<String> items=new ArrayList<>();
    private Context mContext;


    public ListAdapter(ArrayList<String> items, Context mcontext) {
        this.items = items;
        this.mContext = mcontext;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(R.layout.list_item, parent,false);
        TextView itemName=(TextView)convertView.findViewById(R.id.item_name);
        itemName.setText(items.get(position));



        return convertView ;
    }

    public void addItem(String string){
        items.add(string);
        notifyDataSetChanged();
    }
}
